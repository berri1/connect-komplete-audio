FROM rust:bullseye

RUN apt update && apt install gcc-aarch64-linux-gnu -y 

RUN rustup target add aarch64-unknown-linux-gnu

WORKDIR /code
