# Usage

## Cross-compile using docker
```bash
# Build docker image for cross-compilation
sudo docker build -t 'berri:bullseye' .

# Compile for arm target
sudo docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/code -w /code berri:bullseye cargo build --target aarch64-unknown-linux-gnu
```
