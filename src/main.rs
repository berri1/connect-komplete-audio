use rusb::{Context, Device, Error, HotplugBuilder, UsbContext};
use std::boxed::Box;

struct HotPlugHandler;

impl<T: UsbContext> rusb::Hotplug<T> for HotPlugHandler {
    fn device_arrived(&mut self, device: Device<T>) {
        println!("device arrived {:?}", device);
    }

    fn device_left(&mut self, device: Device<T>) {
        println!("device left {:?}", device);
    }
}

impl Drop for HotPlugHandler {
    fn drop(&mut self) {
        println!("HotPlugHandler dropped");
    }
}

fn device_is_connected(vendor_id: u16, product_id: u16) -> bool {
    let devices = match rusb::devices() {
        Ok(devices) => devices,
        Err(error) => panic!("Error: {}", error),
    };

    for device in devices.iter() {
        println!("device: {:#?}", device);
        let device_descriptor = match device.device_descriptor() {
            Ok(device_descriptor) => device_descriptor,
            Err(error) => panic!("Error: {}", error),
        };

        if device_descriptor.vendor_id() == vendor_id
            && device_descriptor.product_id() == product_id
        {
            return true;
        }
    }

    false
}

fn main() -> std::result::Result<(), Error> {
    let native_instruments_vendor_id: u16 = 0x17cc;
    let komplete_audio_6_product_id: u16 = 0x1001;

    while !device_is_connected(native_instruments_vendor_id, komplete_audio_6_product_id) {
        if rusb::has_hotplug() {
            println!("Running hotplug");
            let context = Context::new()?;

            let mut reg = Some(
                HotplugBuilder::new()
                    .enumerate(false)
                    .register(&context, Box::new(HotPlugHandler {}))?,
            );

            loop {
                context
                    .handle_events(Some(std::time::Duration::from_secs(1)))
                    .unwrap();
                if let Some(reg) = reg.take() {
                    context.unregister_callback(reg);
                    break;
                }
            }
        } else {
            println!("Hotplug not supported");
            std::process::exit(1);
        }
        // sleep for 10 seconds
        std::thread::sleep(std::time::Duration::from_secs(10));
    }
    println!("Device connected");
    Ok(())
}
